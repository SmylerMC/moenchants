# MoEnchants

MoEnchants is a Minecraft mod adding new enchantments to the game. The mod tries to stay in line with vanilla Minecraft while bringing more diversity in a balanced game.

## Enchantments
 - Reactivity:
    >A sword enchanted with reactivity will always be ready to respond to a threat.
    
 - Revenge:
    >Make extra damages when taking revenge from someone who hurt you recently.

 - Soulbound:
    >Any tool or armor with the soulbound enchantment will find its way to its owner's enderchest upon his death.

 - Stealing:
    >Are you jalous of someone's equipment? This enchantment is for you!
