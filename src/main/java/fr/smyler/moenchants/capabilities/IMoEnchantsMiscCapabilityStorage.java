package fr.smyler.moenchants.capabilities;

import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;

public class IMoEnchantsMiscCapabilityStorage implements Capability.IStorage<IMoEnchantsMiscCapability> {

	@Override
	public INBT writeNBT(Capability<IMoEnchantsMiscCapability> capability, IMoEnchantsMiscCapability instance,
			Direction side) {
		// TODO Implement writeNBT
		return null;
	}

	@Override
	public void readNBT(Capability<IMoEnchantsMiscCapability> capability, IMoEnchantsMiscCapability instance,
			Direction side, INBT nbt) {
		// TODO Implement readNBT
		
	}
	
	public static IMoEnchantsMiscCapability getDefault() {
		return new IMoEnchantsMiscCapability();
	}

}
