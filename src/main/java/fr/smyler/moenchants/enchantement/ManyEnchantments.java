/**

Copyright 2019 smyler@mail.com
This file is part of the MoEnchants Minecraft  mod.

    The MoEnchants Minecraft mod is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The MoEnchants Minecraft mod is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*/

package fr.smyler.moenchants.enchantement;

import fr.smyler.moenchants.MoEnchantsMod;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraftforge.registries.IForgeRegistry;

/**
 * @author Smyler
 *
 */
public class ManyEnchantments {

	private static final EquipmentSlotType[] ALL = {EquipmentSlotType.MAINHAND, EquipmentSlotType.OFFHAND, EquipmentSlotType.HEAD, EquipmentSlotType.CHEST, EquipmentSlotType.LEGS, EquipmentSlotType.FEET};
	private static final EquipmentSlotType[] ARMOR = {EquipmentSlotType.HEAD, EquipmentSlotType.CHEST, EquipmentSlotType.LEGS, EquipmentSlotType.FEET};
	private static final EquipmentSlotType[] HANDS = {EquipmentSlotType.MAINHAND, EquipmentSlotType.OFFHAND};

	public static final Enchantment AUTOSMELT = new AutoSmeltEnchantment(Enchantment.Rarity.RARE, ManyEnchantments.HANDS);
	public static final Enchantment REACTIVE = new ReactivityEnchantment(Enchantment.Rarity.UNCOMMON, ManyEnchantments.HANDS);
	public static final Enchantment REVENGE = new RevengeEnchantment(Enchantment.Rarity.UNCOMMON, ManyEnchantments.HANDS);
	public static Enchantment SOULBOUND = new SoulBoundEnchantment(Enchantment.Rarity.VERY_RARE, ManyEnchantments.ALL);
	public static Enchantment STEALING = new StealingEnchantment(Enchantment.Rarity.RARE, new EquipmentSlotType[] {EquipmentSlotType.MAINHAND});
	
	public static void registerAll(IForgeRegistry<Enchantment> registry) {
		MoEnchantsMod.LOGGER.info("Registering enchantments!");
		registry.registerAll(
					//AUTOSMELT 		//FIXME HarvestDropsEvent is not being fired in current forge version as the original hooked method has been removed from Minecraft 1.15
					REACTIVE,
					REVENGE,
					SOULBOUND,
					STEALING
				);
	}
	
}
