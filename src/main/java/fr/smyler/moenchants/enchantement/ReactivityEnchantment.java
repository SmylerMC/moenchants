/**

Copyright 2019 smyler@mail.com
This file is part of the MoEnchants Minecraft  mod.

    The MoEnchants Minecraft mod is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The MoEnchants Minecraft mod is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.smyler.moenchants.enchantement;

import java.util.ArrayList;
import java.util.Random;

import fr.smyler.moenchants.MoEnchantsMod;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentType;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

/**
 * @author Smyler
 *
 */
public class ReactivityEnchantment extends Enchantment{

	protected ReactivityEnchantment(Rarity rarityIn, EquipmentSlotType[] slots) {
		super(rarityIn, EnchantmentType.WEAPON, slots);
		this.setRegistryName(MoEnchantsMod.MODID, "reactivity");
		this.name = "moenchants.reactivity";
	}

	public int getMinEnchantability(int enchantmentLevel) {
		return 20;
	}

	public int getMaxEnchantability(int enchantmentLevel) {
		return 50;
	}

	public int getMaxLevel() {
		return 1;
	}

	@SubscribeEvent
	public void onEntityHurt(LivingHurtEvent event) {

		if(!(event.getEntity() instanceof ServerPlayerEntity)) return;
		
		ServerPlayerEntity player = (ServerPlayerEntity) event.getEntity();
		DamageSource source = event.getSource();
		String damage = source.getDamageType();
		if(damage != "mob" && !source.isProjectile()) return; 
		if(EnchantmentHelper.getEnchantmentLevel(ManyEnchantments.REACTIVE, player.inventory.offHandInventory.get(0)) > 0) {
			int current = player.inventory.currentItem;
			ItemStack currentItem = player.inventory.getCurrentItem();
			ItemStack offHandItem = player.inventory.offHandInventory.get(0);
			player.inventory.setInventorySlotContents(current, offHandItem);
			player.inventory.offHandInventory.set(0, currentItem);
			return;
		}

		ArrayList<Integer> slots = ManyEnchantmentsHelper.getSlotsWithEnchantment(player.inventory.mainInventory, ManyEnchantments.REACTIVE);
		Random random = new Random();
		if(slots.size() <= 0) {
			//The item is being moved, ignore it
			return;
		}
		int slot = slots.get(random.nextInt(slots.size()));
		ItemStack active = player.inventory.getCurrentItem();
		player.inventory.setInventorySlotContents(player.inventory.currentItem, player.inventory.getStackInSlot(slot));
		player.inventory.setInventorySlotContents(slot, active);

	}

}
