/**

Copyright 2019 smyler@mail.com
This file is part of the MoEnchants Minecraft  mod.

    The MoEnchants Minecraft mod is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The MoEnchants Minecraft mod is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.smyler.moenchants.enchantement;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import fr.smyler.moenchants.MoEnchantsMod;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentType;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

/**
 * @author Smyler
 *
 */
public class StealingEnchantment extends Enchantment {

	/**
	 * @param rarityIn
	 * @param typeIn
	 * @param slots
	 */
	protected StealingEnchantment(Rarity rarityIn, EquipmentSlotType[] slots) {
		super(rarityIn, EnchantmentType.WEAPON, slots);
		this.setRegistryName(MoEnchantsMod.MODID, "stealing");
		this.name = MoEnchantsMod.MODID + ".stealing";
	}

	@SubscribeEvent
	public void onLivingHurt(LivingHurtEvent event) {
		Entity source = event.getSource().getTrueSource();
		if(source == null) return;
		if(!(source instanceof LivingEntity)) return;
		LivingEntity sourceEntity = (LivingEntity) source;
		int enchantLevel = EnchantmentHelper.getEnchantmentLevel(ManyEnchantments.STEALING, sourceEntity.getHeldItemMainhand());
		if(enchantLevel <= 0) return;
		Random random = new Random();
		if(random.nextFloat() + enchantLevel * 0.10f < 1) return;
		LivingEntity target = event.getEntityLiving();
		ItemStack stack = null;
		ServerPlayerEntity player = null;
		if(target instanceof ServerPlayerEntity) {
			player = (ServerPlayerEntity) target;
			ArrayList<Integer> mainSlots = this.getNonEmptySlots(player.inventory.mainInventory);
			ArrayList<Integer> armorSlots = this.getNonEmptySlots(player.inventory.armorInventory);
			ArrayList<Integer> offHandSlots = this.getNonEmptySlots(player.inventory.offHandInventory);
			int slotCount = mainSlots.size() + armorSlots.size() + offHandSlots.size();
			if(slotCount <= 0) return;
			int slot = random.nextInt(slotCount);
			if(slot < mainSlots.size()) {
				stack = player.inventory.mainInventory.get(mainSlots.get(slot));
			} else if(slot < mainSlots.size() + armorSlots.size()) {
				stack = player.inventory.armorInventory.get(armorSlots.get(slot - mainSlots.size()));
			} else {
				stack = player.inventory.offHandInventory.get(offHandSlots.get(slot - mainSlots.size() - armorSlots.size()));
			}
		} else {
			ArrayList<ItemStack> items = new ArrayList<ItemStack>();
			target.getEquipmentAndArmor().forEach(item -> {
															if(!item.isEmpty()) items.add(item);
														});
			if(items.size() <= 0) return;
			stack = items.get(random.nextInt(items.size()));
		}
		MoEnchantsMod.LOGGER.info(stack);
		if(stack == null) return;
		ItemStack dropping = stack.copy();
		stack.shrink(1);
		dropping.setCount(1);
		if(player != null) {
			player.dropItem(dropping, true, true);
		} else {
			target.entityDropItem(dropping);
		}
	}

	private ArrayList<Integer> getNonEmptySlots(List<ItemStack> inventory) {
		ArrayList<Integer> slots = new ArrayList<Integer>();
		for(int i=0; i< inventory.size(); i++) {
			if(!inventory.get(i).isEmpty()) slots.add(i);
		}
		return slots;
	}
	
	   /**
	    * Returns the minimal value of enchantability needed on the enchantment level passed.
	    */
	   public int getMinEnchantability(int enchantmentLevel) {
	      return 10 + 20 * (enchantmentLevel - 1);
	   }

	   public int getMaxEnchantability(int enchantmentLevel) {
	      return super.getMinEnchantability(enchantmentLevel) + 50;
	   }

	   /**
	    * Returns the maximum level that the enchantment can have.
	    */
	   public int getMaxLevel() {
	      return 5;
	   }

}
