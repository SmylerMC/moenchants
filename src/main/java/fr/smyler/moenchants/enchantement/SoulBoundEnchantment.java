/**

Copyright 2019 smyler@mail.com
This file is part of the MoEnchants Minecraft  mod.

    The MoEnchants Minecraft mod is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The MoEnchants Minecraft mod is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.smyler.moenchants.enchantement;

import java.util.ArrayList;

import fr.smyler.moenchants.MoEnchantsMod;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentType;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.EnderChestInventory;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

/**
 * @author Smyler
 *
 */
public class SoulBoundEnchantment extends Enchantment{

	/**
	 * @param rarityIn
	 * @param slots
	 */
	protected SoulBoundEnchantment(Rarity rarityIn, EquipmentSlotType[] slots) {
		super(rarityIn, EnchantmentType.ALL, slots);
		this.setRegistryName(MoEnchantsMod.MODID, "souldbound");
		this.name = "moenchants.soulbound";
	}

	/**
	 * Returns the minimal value of enchantability needed on the enchantment level passed.
	 */
	public int getMinEnchantability(int enchantmentLevel) {
		return 20;
	}

	public int getMaxEnchantability(int enchantmentLevel) {
		return 50;
	}

	/**
	 * Returns the maximum level that the enchantment can have.
	 */
	public int getMaxLevel() {
		return 1;
	}

	@SubscribeEvent
	public void onLivingDies(LivingDeathEvent event) {
		if(!(event.getEntityLiving() instanceof ServerPlayerEntity)) return;

		ServerPlayerEntity player = (ServerPlayerEntity) event.getEntityLiving();
		PlayerInventory inventory = player.inventory;
		EnderChestInventory enderInventory = player.getInventoryEnderChest();
		this.processInventory(inventory.offHandInventory, enderInventory);
		this.processInventory(inventory.armorInventory, enderInventory);
		this.processInventory(inventory.mainInventory, enderInventory);

	}

	/**
	 * Moves the items with the enchantment to the destination inventory if possible
	 * 
	 * @param source
	 * @param destination
	 */
	private void processInventory(NonNullList<ItemStack> source, Inventory destination) {
		ArrayList<Integer> slots = ManyEnchantmentsHelper.getSlotsWithEnchantment(source, ManyEnchantments.SOULBOUND);
		for(int i=0; i<slots.size(); i++) {
			int slot = slots.get(i);
			ItemStack stack = destination.addItem(source.get(slot));
			source.set(slot, stack);
		}
	}
}
