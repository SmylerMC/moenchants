/**

Copyright 2019 smyler@mail.com
This file is part of the MoEnchants Minecraft  mod.

    The MoEnchants Minecraft mod is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The MoEnchants Minecraft mod is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*/

package fr.smyler.moenchants.enchantement;

import java.util.ArrayList;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;

/**
 * @author Smyler
 *
 */
public class ManyEnchantmentsHelper {

	public static ArrayList<Integer> getSlotsWithEnchantment(NonNullList<ItemStack> inventory, Enchantment enchant) {
		ArrayList<Integer> slots = new ArrayList<Integer>();
		for(int i = 0; i < inventory.size(); ++i) {
			ItemStack stack = inventory.get(i);
			if (!stack.isEmpty() && stack.isEnchanted() && EnchantmentHelper.getEnchantmentLevel(enchant, stack) > 0) {
				slots.add(i);
			}
		}

		return slots;
	}


}
