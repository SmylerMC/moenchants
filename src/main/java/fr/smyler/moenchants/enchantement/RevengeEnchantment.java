package fr.smyler.moenchants.enchantement;

import fr.smyler.moenchants.MoEnchantsMod;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class RevengeEnchantment extends Enchantment {

	//TODO Make this configurable
	private static int REVENGE_TIME = 200; //Max revenge time in ticks

	public RevengeEnchantment(Rarity rarityIn, EquipmentSlotType[] slots) {
		super(rarityIn, EnchantmentType.WEAPON, slots);
		this.setRegistryName(MoEnchantsMod.MODID, "revenge");
		this.name = MoEnchantsMod.MODID + ".revenge";
	}

	@Override
	public int getMaxLevel() {
		return 5;
	}

	@Override
	public int getMinEnchantability(int enchantmentLevel) {
		return 1 + (enchantmentLevel - 1) * 10;
	}

	@Override
	public int getMaxEnchantability(int enchantmentLevel) {
		return this.getMinEnchantability(enchantmentLevel) + 15;
	}
	
	@SubscribeEvent
	public void onLivingHurt(LivingHurtEvent event) {
		if(!(event.getSource().getTrueSource() instanceof LivingEntity)) return;
		LivingEntity source = (LivingEntity) event.getSource().getTrueSource();
		if(source.getRevengeTarget() != event.getEntityLiving()) return;
		int lvl = EnchantmentHelper.getEnchantmentLevel(ManyEnchantments.REVENGE, source.getHeldItemMainhand());
		if(lvl <= 0) return;
		int time = source.ticksExisted - source.getRevengeTimer();
		if(time > RevengeEnchantment.REVENGE_TIME) return;
		float damage = event.getAmount();
		event.setAmount(damage * (1 + 0.1f * lvl));
	}

}
