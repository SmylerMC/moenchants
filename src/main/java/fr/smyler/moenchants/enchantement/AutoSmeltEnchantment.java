/**

Copyright 2019 smyler@mail.com
This file is part of the MoEnchants Minecraft  mod.

    The MoEnchants Minecraft mod is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The MoEnchants Minecraft mod is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*/
package fr.smyler.moenchants.enchantement;

import java.util.List;

import fr.smyler.moenchants.MoEnchantsMod;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentType;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.world.World;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

/**
 * @author Smyler
 *
 */
public class AutoSmeltEnchantment extends Enchantment {

	/**
	 * @param rarityIn
	 * @param slots
	 */
	protected AutoSmeltEnchantment(Rarity rarityIn, EquipmentSlotType[] slots) {
		super(rarityIn, EnchantmentType.DIGGER, slots);
		this.setRegistryName(MoEnchantsMod.MODID, "autosmelt");
		this.name = "enchantment.autosmelt";
	}
	
	@Override
	protected boolean canApplyTogether(Enchantment enchantment) {
		return !(enchantment == Enchantments.FORTUNE);
	}
	
	@SubscribeEvent
	public void onBlockHarvested(BlockEvent.HarvestDropsEvent event) {
		
		//FIXME HarvestDropsEvent is not being fired in current forge version as the original hooked method has been removed from Minecraft 1.15
		
		//Autosmelt enchantment
		if(!event.getWorld().isRemote()) return;
		if(event.getHarvester() == null) return; // This is not a player
		PlayerEntity player = event.getHarvester();
		ItemStack tool = player.getHeldItemMainhand();
		if(EnchantmentHelper.getEnchantmentLevel(ManyEnchantments.AUTOSMELT, tool) <= 0) return;
		List<ItemStack> drops = event.getDrops();
		World world = (World) event.getWorld();
		for(int i=0; i<drops.size(); i++) {
			ItemStack item = drops.get(i);
			int count = item.getCount();
			Inventory inv = new Inventory(item);
            IRecipe<?> irecipe = world.getRecipeManager().getRecipe(IRecipeType.SMELTING, inv, world).orElse(null);
            if(irecipe == null) continue;
            ItemStack result = irecipe.getRecipeOutput();
            result.setCount(count);
            drops.set(i, result);
		}
	}
	

}
