/**

Copyright 2019 smyler@mail.com
This file is part of the MoEnchants Minecraft  mod.

    The MoEnchants Minecraft mod is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The MoEnchants Minecraft mod is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*/

package fr.smyler.moenchants;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.smyler.moenchants.capabilities.IMoEnchantsMiscCapability;
import fr.smyler.moenchants.capabilities.IMoEnchantsMiscCapabilityStorage;
import fr.smyler.moenchants.enchantement.ManyEnchantments;
import net.minecraft.block.Block;
import net.minecraft.enchantment.Enchantment;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.InterModEnqueueEvent;
import net.minecraftforge.fml.event.lifecycle.InterModProcessEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

/**
 * @author Smyler
 *
 */
@Mod(MoEnchantsMod.MODID)
public class MoEnchantsMod {
    
	public static final String MODID = "moenchants";
    public static final Logger LOGGER = LogManager.getLogger(MoEnchantsMod.MODID);

    public MoEnchantsMod() {
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::enqueueIMC);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::processIMC);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::doClientStuff);
        MinecraftForge.EVENT_BUS.register(this);
        MinecraftForge.EVENT_BUS.register(ManyEnchantments.AUTOSMELT);
        MinecraftForge.EVENT_BUS.register(ManyEnchantments.REACTIVE);
        MinecraftForge.EVENT_BUS.register(ManyEnchantments.REVENGE);
        MinecraftForge.EVENT_BUS.register(ManyEnchantments.SOULBOUND);
        MinecraftForge.EVENT_BUS.register(ManyEnchantments.STEALING);

    }

    private void setup(final FMLCommonSetupEvent event) {
    	CapabilityManager.INSTANCE.register(IMoEnchantsMiscCapability.class, new IMoEnchantsMiscCapabilityStorage(), IMoEnchantsMiscCapabilityStorage::getDefault);
    }

    private void doClientStuff(final FMLClientSetupEvent event) {
    }

    private void enqueueIMC(final InterModEnqueueEvent event) {
    }

    private void processIMC(final InterModProcessEvent event) {

    }

    @SubscribeEvent
    public void onServerStarting(FMLServerStartingEvent event) {
    }


    @Mod.EventBusSubscriber(bus=Mod.EventBusSubscriber.Bus.MOD)
    public static class RegistryEvents {
        @SubscribeEvent
        public static void onBlocksRegistry(final RegistryEvent.Register<Block> blockRegistryEvent) {
        }
        @SubscribeEvent
        public static void onEnchantemetnRegistry(final RegistryEvent.Register<Enchantment> enchantRegistryEvent) {
        	ManyEnchantments.registerAll(enchantRegistryEvent.getRegistry());
        }
    }
}
